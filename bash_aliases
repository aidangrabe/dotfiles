#!/bin/bash

alias ..='cd ../'
alias ...='cd ../../'
alias ....='cd ../../../'

alias cdb='cd ~/bin'
alias cdd='cd ~/Downloads'
alias cdp='cd ~/python'
alias cds='cd ~/bash'
alias cdj='cd ~/java'
alias cdw='cd /var/www'

alias la='ls -a'                    # list err'-thang
alias ll='ls -l'                    # list perms
alias lsd='ls -d | grep ^d'        # list directories
alias lss='ls -l | grep ^l'        # list symlinks

alias edit='vim'
alias v='vim'
alias vi='vim'
alias edit="vim"
alias grep="grep --colour=auto"
alias open="xdg-open"

alias gs="git status"
alias ga="git add ."
alias gc="git commit"
alias gl="git log --graph"
alias gd="git diff"

alias xclip="xclip -selection clipboard"    # copy to clipboard
