#!/bin/bash

DIR_DOT=~/.dotfiles             # directory of dotfiles
DIR_BKP=~/.dotfiles.old         # backup directory for dotfiles
FILES=("$(ls $DIR_DOT)")        # the dotfiles themselves

mkdir -p $DIR_BKP
cd $HOME



for FILE in $FILES; do
    [ $FILE == $0 ] && continue         # exclude the install script
	[ -e ".$FILE" ] && mv ".$FILE" "${DIR_BKP}/.${FILE}"   # move old dotfiles to backup
    ln -s "${DIR_DOT}/${FILE}" ~/.$FILE # symlink new dotfiles
done

exit 0
