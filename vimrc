" visual stuff
set background=dark
set cursorline
set ruler
syntax on

" scrolling
nnoremap <C-Down> <C-e>
nnoremap <C-Up> <C-y>

map <Home> ^

set history=100

set wildchar=<TAB>
set wildmenu
set wildignore=*.pyc,*.jar

" indentation
set autoindent
set smartindent

" windows
nmap <M-Left> <c-w>h
nmap <M-Up> <c-w>k
nmap <M-Right> <c-w>l
nmap <M-Down> <c-w>j

" tabs
set bs=2
set expandtab
set shiftwidth=4
set smarttab
set softtabstop=4
set tabstop=4

" searching
set hlsearch
set incsearch
set ignorecase
set smartcase
set pastetoggle=<F12>
"nnoremap <c-l> :noh<return>         " turn off highlighting when CTRL+L is hit
nnoremap <silent> <return> :noh<return>     " turn off highlighting when Return is hit

" colorscheme
colorscheme molokai
set t_Co=256

" extra syntax hihglighting
au BufNewFile,BufRead *.less set filetype=less
